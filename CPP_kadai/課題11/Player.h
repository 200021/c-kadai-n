class Player {
public:
	int hp, atk, def;

public:
	Player();
	void DispHp();
	int Attack(int);
	void Damage(int);
	int GetDef();
	bool IsDead();
};
#include "Status.h"
//レベル
void Status::SetLv(int i)
{
	lv = i;
}
//パラメータ
void Status::Calc()
{
	hp = lv * lv + 50;
	atk = lv * 10;
	def = lv * 9;
}
//HP
int Status::GetHp()
{
	return hp;
}
//攻撃力値
int Status::GetAtk()
{
	return atk;
}
//防御力値
int Status::GetDef()
{
	return def;
}
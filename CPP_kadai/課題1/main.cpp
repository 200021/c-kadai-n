#include "Sample.h"
//メイン関数
int main()
{
	//SampleClass クラスのインスタンス
	SampleClass a;

	//３つのメンバ関数を呼び出す
	a.Input();
	a.Plus();
	a.Disp();
}

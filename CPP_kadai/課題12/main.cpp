#include "Sample.h"

SampleClass* a;

int main()
{
	//インスタンス
	a = new SampleClass;
	//メンバ関数
	a->Input();
	a->Plus();
	a->Disp();
	//インスタンス
	delete a;
}
//クラスを宣言

class SampleClass {
	//データメンバ
protected:
	int a;
	int b;
	int c;

	//メンバ関数
public:
	void Input();
	void Plus();
	void Disp();
};

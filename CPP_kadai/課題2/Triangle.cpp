#include <iostream>
#include "TriangleClass.h"

//底辺と高さの値を代入
void TriangleClass::Input()
{
	teihen = 20.0f;
	takasa = 15.0f;
}

//三角形の面積
void TriangleClass::Calc()
{
	menseki = teihen * takasa / 2.0f;
}

//結果を出力
void TriangleClass::Disp()
{
	std::cout << "三角形の面積＝" << menseki << "\n";
}
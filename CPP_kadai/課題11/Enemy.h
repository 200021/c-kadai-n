class Enemy {
public:
	int hp, atk, def;

public:
	Enemy();
	void DispHp();
	int Attack(int);
	void Damage(int);
	int GetDef();
	bool IsDead();
};